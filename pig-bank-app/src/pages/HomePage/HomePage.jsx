import React from 'react'
import Header from '../../components/Header/Header'
import Button from '../../components/Button/Button'
import Breaker from '../../components/Breaker/Breaker'
import Footer from '../../components/Footer/Footer'
import Circles from '../../components/Circles/Circles'

import './HomePage.css'

import piggy from '../../assets/piggy.png'
import people from '../../assets/people-in-gold.svg'

import {useHistory} from 'react-router-dom'


function HomePage() {
    let history = useHistory()
    return (
        <>
            <Header />

            <info-field>
                <img src={piggy} alt="imagem de um cofrinho estilo porco" />
                <text-field>
                    <h2>A mais nova alternativa de banco digital chegou!</h2>
                    <p>Feito para caber no seu bolsoe otimizar seu tempo. O <strong>PigBank</strong> veio pra ficar!</p>
                    <Button clicking={()=>history.push('/pagina-em-construcao')} buttonWord="Abra sua conta" buttonClass="pink-background"/>
                </text-field>
            </info-field>

            <Breaker breakerWord="Fique por dentro!"/>

            <info-field>
                <text-field>
                    <p>
                    Ao contrário do ditado popular, por aqui, quem se mistura com porco não come farelo!
                    Conheça nossa plataforma exclusivamente dedicada para ampliar o seu patrimônio.
                    </p>
                </text-field>
                <img src={people} alt="an image of two person over gold" />
                <Button clicking={()=>history.push('cotacao')} buttonWord="Cotação Moedas" buttonClass="pink-background"/>
            </info-field>
           <Circles />
            <Footer />
        </>
    )
}

export default HomePage
