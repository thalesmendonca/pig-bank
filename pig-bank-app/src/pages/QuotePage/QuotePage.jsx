import React, { useEffect, useState} from 'react'

import Header from '../../components/Header/Header'
import Breaker from '../../components/Breaker/Breaker'
import Footer from '../../components/Footer/Footer'
import Card from '../../components/Card/Card'

import {getListCoin} from '../../service/api'


function QuotePage() {
    const [coinList, setCoinList] = useState([])
    
    useEffect(() => {
        getListCoin(setCoinList)
    }, [])

    return (
        <>
            <Header />
            <Breaker breakerWord='Cotação Moedas'/>
            {coinList.map((coin)=><Card info={coin}/>)}
            <Footer />
        </>
    )
}

export default QuotePage
