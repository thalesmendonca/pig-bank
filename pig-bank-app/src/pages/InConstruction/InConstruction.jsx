import React from 'react'
import Header from '../../components/Header/Header'
import Button from '../../components/Button/Button'
import Footer from '../../components/Footer/Footer'

import Builder from '../../assets/builder-piggy.svg'

import './InConstruction.css'

import {useHistory} from 'react-router-dom'
function InConstruction({}) {
    let history = useHistory()
    return (
        <>
            <Header />

            <div className="in-construction">
                <h2>Em Construção</h2>
                <img src={Builder} alt="" />
                <p>
                Parece que essa página ainda não foi implementada... 
Tente novamente mais tarde!
                </p>
                <Button clicking={()=>history.push('/')} buttonWord="VOLTAR" buttonClass="pink-background"></Button>
            </div>

            <Footer />
        </>
    )
}

export default InConstruction
