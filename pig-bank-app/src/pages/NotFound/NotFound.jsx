import React from 'react'
import Header from '../../components/Header/Header'
import Button from '../../components/Button/Button'
import Footer from '../../components/Footer/Footer'

import confusedPiggy from '../../assets/confused-piggy.svg'

import './NotFound.css'

import {useHistory} from 'react-router-dom'

function NotFound() {
    let history = useHistory()
    return (
        <>
            <Header />

            <div className="not-found">
                <h2>ERRO 404</h2>
                <img src={confusedPiggy} alt="" />
                <p>
                Ops! Parece que o endereço que você digitou está incorreto...
                </p>
                <Button clicking={()=>history.push('/')} buttonWord="VOLTAR" buttonClass="pink-background"></Button>
            </div>

            <Footer />
        </>
    )
}

export default NotFound
