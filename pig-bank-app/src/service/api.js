import axios from 'axios'

function getListCoin(setCoinList) {
    axios.get('https://economia.awesomeapi.com.br/json/all')
    .then((resp) => {
        let list = []
        Object.keys(resp.data).forEach((item)=>{
            list.push(resp.data[item])
        })
        setCoinList(list)
    })
    .catch((err) => console.error(err))
}

export {getListCoin}