import React from 'react'
import { BrowserRouter as Router,  Switch, Route, Redirect } from 'react-router-dom'
import HomePage from '../pages/HomePage/HomePage'
import QuotePage from '../pages/QuotePage/QuotePage'
import InConstruction from '../pages/InConstruction/InConstruction'
import NotFound from '../pages/NotFound/NotFound'

function Routes() {
    return (
        <Router>
            <Switch>
                <Route exact path='/'>
                    <HomePage />
                </Route>
                <Route exact path='/cotacao'>
                    <QuotePage />
                </Route>
                <Route exact path='/pagina-em-construcao'>
                    <InConstruction />
                </Route>
                <Route exact path='/404'>
                    <NotFound />
                </Route>
                <Route path ='/'>
                    <Redirect to ='/404'/>
                </Route>
            </Switch>
        </Router>
    )
}

export default Routes