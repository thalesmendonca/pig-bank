import React from 'react'
import './Card.css'




function Card({ info }) {
    return (
        <div className='card'>
            <div>
                <p>{info['name']}</p>
                <h2>{info['code']}</h2>
            </div>
            <div>
                <p>Máxima: R$:{info['high']}</p>
                <p>Mínima: R$:{info['low']}</p>
                <p>{info['create_date']}</p>
            </div>
        </div>
    )
}

export default Card