import React from 'react'
import circles from '../../assets/circles.svg'
import './Circles.css'

function Circles() {
    return (
        <div className="circle">
            <img src={circles} alt="" />
        </div>
    )
}

export default Circles
