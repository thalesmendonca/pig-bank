import React from 'react'
import Button from '../Button/Button'

import './Header.css'

import { useHistory } from 'react-router-dom'




function Header() {
    const history = useHistory()

    return (
        <header>
            <h1 onClick ={()=> history.push('/')}> Pigbank </h1>
            <buttons-field>
                <Button clicking={() => history.push('/cotacao')} buttonWord='Cotação Moedas' buttonClass='pink-background'> </Button>
                <Button clicking={() => history.push('/pagina-em-construcao')} buttonWord='Acessar' buttonClass='white-background'>  </Button>
            </buttons-field>
        </header>
    )
}

export default Header
